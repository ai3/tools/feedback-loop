import unittest
from feedbackloop.parse import MailScanner
from feedbackloop.test import TEST_MESSAGE


class MailScannerTest(unittest.TestCase):

    def test_parse_message_ok(self):
        ms = MailScanner('server', 'user', 'password')
        entry = ms._parse_message(TEST_MESSAGE)
        self.assertTrue(entry)
        self.assertEqual('foo@example.net', entry.sender)
        self.assertEqual('abusedesk@example.com', entry.reporter)
        self.assertFalse(entry.is_list)
