import unittest
from feedbackloop.arf import ARFMessage
from feedbackloop.test import TEST_MESSAGE


class ARFTest(unittest.TestCase):

    def test_parse_arf(self):
        arf = ARFMessage(TEST_MESSAGE)
        report = arf.get_feedback_report()
        self.assertTrue(report)
        self.assertEqual('1.2.3.4', report.get_source_ip())
