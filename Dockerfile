FROM debian:stable-slim AS build
ADD . /src
WORKDIR /src
RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends \
        python3-setuptools python3-pip && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/* && \
    python3 setup.py bdist_wheel

FROM registry.git.autistici.org/ai3/docker/s6-overlay-lite:master
COPY --from=build /src/dist/*.whl /tmp/wheels/
COPY conf/ /etc/
RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends \
        python3-setuptools python3-pip python3-virtualenv && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/* && \
    virtualenv /virtualenv && \
    cd /tmp/wheels && /virtualenv/bin/pip3 install *.whl && rm -fr /tmp/wheels
COPY feedbackloop.sh /virtualenv/bin/feedbackloop
