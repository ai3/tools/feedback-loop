from setuptools import setup, find_packages

setup(name='feedbackloop',
      version='1.0',
      description='A/I Spam Feedback Loop Management',
      author='A/I',
      author_email='info@autistici.org',
      url='https://git.autistici.org/ai3/tools/feedback-loop',
      packages=find_packages(),
      install_requires=[
          'Flask', 'cheroot', 'sqlalchemy', 'Flask-SQLAlchemy',
      ],
      zip_safe=False,
      include_package_data=True,
)
