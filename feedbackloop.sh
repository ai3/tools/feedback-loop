#!/bin/sh

export FLASK_APP=feedbackloop.main:create_app

exec /virtualenv/bin/flask "$@"
