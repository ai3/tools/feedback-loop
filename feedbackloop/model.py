from .app import db


class FeedbackEntry(db.Model):
    __tablename__ = 'feedback'
    id = db.Column(db.Integer, primary_key=True)
    sender = db.Column(db.Text, index=True)
    reporter = db.Column(db.Text)
    is_list = db.Column(db.Boolean, index=True)
    message = db.Column(db.Text)
    timestamp = db.Column(db.DateTime)

    def to_api_dict(self):
        return {
            'id': self.id,
            'sender': self.sender,
            'reporter': self.reporter,
            'is_list': self.is_list,
            'timestamp': self.timestamp.strftime('%Y-%m-%dT%H:%M:%SZ'),
        }
