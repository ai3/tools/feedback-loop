from cheroot import wsgi
from datetime import datetime, timedelta
import click

from .app import app, db

# Import all views by side effect.
from . import views
from .model import FeedbackEntry
from .parse import MailScanner
from .debug import DebugMbox


def create_app(config=None):
    app.config.from_envvar('APP_CONFIG', silent=True)
    if config:
        app.config.update(config)
    return app


@app.cli.command('ingest')
@click.option('--unseen', is_flag=True, default=False)
@click.option('--limit', type=int, default=0)
def ingest(unseen, limit):
    """Read and ingest new messages from the IMAP mailbox."""
    with app.app_context():
        with MailScanner(
                app.config['IMAP_SERVER'],
                app.config['IMAP_USERNAME'],
                app.config['IMAP_PASSWORD'],
                app.config.get('IMAP_REMOVE_MESSAGES', True),
                app.config.get('LIST_DOMAINS'),
                DebugMbox(app.config.get('DEBUG_MBOX_PATH')),
        ) as scanner:
            for entry in scanner.scan(unseen, limit):
                db.session.add(entry)
        db.session.commit()


@app.cli.command('expire')
@click.option('--days', type=int, default=30)
def expire(days):
    """Expire old entries from the database."""
    cutoff = datetime.now() - timedelta(days)
    with app.app_context():
        FeedbackEntry.query.filter(
            FeedbackEntry.timestamp < cutoff
        ).delete()
        db.session.commit()


@app.cli.command('server')
@click.option('--addr', default='0.0.0.0')
@click.option('--port', type=int, default=3030)
def server(addr, port):
    db.create_all()

    wsgi.Server((addr, port), app).start()
