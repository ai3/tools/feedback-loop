feedback-loop
===


This small server receives spam feedback loop messages, as sent from
large email providers in ARF format, and stores the entries in a
database so that you can see aggregates and details via a web
interface. Records are periodically expunged from the database if they
are older than a certain time period.

Due to the common tendency to report mailing list emails as spam, the
tool tries to identify reports for mailing lists emails, and presents
their statistics separately from those about emails from ordinary
users (which are often indicative of an account takeover). Details
(i.e. the message body) for mailing list emails are not saved to the
database to save space and effort.

The tool reads emails from an IMAP account, configured via the
`IMAP_SERVER`, `IMAP_USERNAME` and `IMAP_PASSWORD` parameters (SSL is
assumed). Emails are deleted from the remote account once the reports
have been successfully saved to the database.

The HTTP server can be started with:

```
feedbackloop server --port=1234
```

There are two cron jobs that need to be run periodically:

```
# Fetch new reports.
feedbackloop ingest

# Expire old records.
feedbackloop expire --days=30
```

