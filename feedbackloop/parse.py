#!/usr/bin/python3

import imaplib
import re
import time
from datetime import datetime
from email.utils import parsedate
from feedbackloop.arf import ARFMessage

from .model import FeedbackEntry


class IMAPError(Exception):
    pass


_bounces_rx = re.compile(r'-(bounces|owner)@')


def _normalize_addr(addr):
    """Normalize an address, handling <> syntax.

    The returned boolean is true if the address looked like
    a mailman bounce address.
    """
    m = re.search(r'<([^>]+)>', addr)
    if m:
        addr = m[1]
    if _bounces_rx.search(addr):
        return _bounces_rx.sub('', addr), True
    return addr, False


def _find_authenticated_sender(hdr_list):
    for name, value in hdr_list:
        if name != 'Received':
            continue
        m = re.search(r'\(Authenticated sender: ([^)]+)\)', value)
        if m:
            return m[1]


def _hdr(hdr_list, name):
    for k, v in hdr_list:
        if k == name:
            return v


class MailScanner():
    """Read messages from a remote IMAP folder and remove them.

    Operates as a context manager, messages scanned are only removed
    if the inner block executes successfully.
    """

    def __init__(self, server, username, password, remove=True,
                 list_domains=None, debug_mbox=None):
        self._server = server
        self._username = username
        self._password = password
        self._remove = remove
        self._to_delete = []
        self._conn = None
        self._debug_mbox = debug_mbox
        if list_domains:
            self._list_id_rx = re.compile(
                r'<(.+)\.(%s)>' % (
                    '|'.join(map(re.escape, list_domains))))
        else:
            self._list_id_rx = None

    def __enter__(self):
        self._conn = imaplib.IMAP4_SSL(self._server)
        self._conn.socket().settimeout(30)
        try:
            self._conn.login(self._username, self._password)
            self._conn.select('INBOX')
        except:
            try:
                self._conn.close()
            except:
                pass
            raise
        return self

    def __exit__(self, exc_type, value, traceback):
        if self._remove and not exc_type:
            for uid in self._to_delete:
                self._conn.uid('store', uid.encode('utf-8'), '+FLAGS', '\\Deleted')
            self._conn.expunge()
        try:
            self._conn.close()
        except:
            pass

    def _list_name_from_id(self, list_id):
        if self._list_id_rx:
            m = self._list_id_rx.search(list_id)
            if m:
                return f'{m[1]}@{m[2]}'
        return None

    def _scan_mailbox(self, unseen=False, limit=None):
        i = 0
        res, data = self._conn.uid('search', None,
                                   '(UNSEEN)' if unseen else 'ALL')
        if res != 'OK':
            raise IMAPError(res)
        for uid in data[0].decode('utf-8').split():
            res, response = self._conn.uid('fetch', uid, '(RFC822)')
            if res != 'OK':
                raise IMAPError(res)
            yield uid, response[0][1]
            i += 1
            if limit and i > limit:
                break

    def scan(self, unseen, limit=None):
        for uid, raw_msg in self._scan_mailbox(unseen, limit):
            try:
                entry = self._parse_message(raw_msg)
                self._to_delete.append(uid)
                yield entry
            except Exception as e:
                print(f'error: {e}')
                if self._debug_mbox:
                    self._debug_mbox.write_message(raw_msg)

    def _parse_message(self, raw_msg):
        # Parse the ARF message body.
        msg = ARFMessage(raw_msg)
        report_sender, _ = _normalize_addr(
            _hdr(msg.get_message_headers(), 'From'))

        # Try different ways of finding the original sender. We
        # look at a few possible things, in order of increasing
        # priority:
        #
        # - the From of the original message
        # - the From reported in the feedback-report attachment
        # - the authenticated sender from the Received headers
        #
        # We also attempt to detect if the sender is a mailing
        # list by looking for a "List-Id" header, or a "-bounces"
        # sender.
        hdrs = msg.get_original_message_headers()
        is_list = False
        timestamp = datetime.now()
        original_from = None

        if hdrs:
            original_from = _hdr(hdrs, 'From')
            timestamp = datetime.fromtimestamp(
                time.mktime(parsedate(_hdr(hdrs, 'Date'))))

        feedback_report = msg.get_feedback_report()
        if feedback_report:
            print('feedback report: %s - %s' % (
                feedback_report.get_feedback_type(),
                feedback_report.get_original_mail_from()))
            original_from = feedback_report.get_original_mail_from()
            arrival_date = feedback_report.get_arrival_date()
            if arrival_date:
                timestamp = datetime.fromtimestamp(
                    time.mktime(parsedate(arrival_date)))

        if hdrs:
            auth_sender = _find_authenticated_sender(hdrs)
            if auth_sender:
                print(f'found authenticated sender: {auth_sender}')
                original_from = auth_sender

        if hdrs:
            list_id = _hdr(hdrs, 'List-Id')
            if list_id:
                list_name = self._list_name_from_id(list_id)
                if list_name:
                    original_from = list_name
                    is_list = True

        if not original_from:
            raise Exception('unable to extract original sender')

        original_from, maybe_list = _normalize_addr(original_from)
        if maybe_list and not is_list:
            is_list = maybe_list

        print(f'original from: {original_from}, list={is_list}')
        return FeedbackEntry(
            sender=original_from,
            reporter=report_sender,
            is_list=is_list,
            timestamp=timestamp,
            message=raw_msg,
        )
