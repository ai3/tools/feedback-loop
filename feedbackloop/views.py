from flask import request, render_template, abort, jsonify
from sqlalchemy import func, text
from .app import app, db
from .model import FeedbackEntry


@app.route('/report/<report_id>')
def show_report(report_id):
    report = FeedbackEntry.query.get(report_id)
    if not report:
        abort(404)
    return render_template('report.html', report=report)


@app.route('/by_sender/<sender>')
def by_sender(sender):
    reports = FeedbackEntry.query.filter(
        FeedbackEntry.sender == sender).order_by(
            FeedbackEntry.timestamp.desc())
    if not reports:
        abort(404)
    return render_template('sender.html',
                           sender=sender,
                           reports=reports)


@app.route('/api/by_sender', methods=('POST',))
def api_by_sender():
    sender = request.json.get('sender')
    if not sender:
        abort(400)
    reports = FeedbackEntry.query.filter(
        FeedbackEntry.sender == sender).order_by(
            FeedbackEntry.timestamp.desc())
    return jsonify({
        'reports': [x.to_api_dict() for x in reports],
    })


@app.route('/')
def index():
    n = int(request.args.get('n', '20'))
    top_user_senders = db.session.query(
        FeedbackEntry.sender,
        func.count(FeedbackEntry.id).label('count')).filter(
            FeedbackEntry.is_list == False).group_by(
                FeedbackEntry.sender).order_by(text('count DESC')).limit(n)
    top_list_senders = db.session.query(
        FeedbackEntry.sender,
        func.count(FeedbackEntry.id).label('count')).filter(
            FeedbackEntry.is_list == True).group_by(
                FeedbackEntry.sender).order_by(text('count DESC')).limit(n)
    top_reporters = db.session.query(
        FeedbackEntry.reporter,
        func.count(FeedbackEntry.id).label('count')).group_by(
            FeedbackEntry.reporter).order_by(text('count DESC'))

    return render_template('index.html',
                           top_user_senders=top_user_senders,
                           top_list_senders=top_list_senders,
                           top_reporters=top_reporters)
