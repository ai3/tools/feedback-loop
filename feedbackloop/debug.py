import mailbox


class DebugMbox(object):

    def __init__(self, path):
        self._path = path

    def write_message(self, msg):
        if not self._path:
            return
        mbox = mailbox.mbox(self._path)
        mbox.lock()
        try:
            mbox.add(msg)
        finally:
            mbox.unlock()
            mbox.close()
