import mock
from flask_testing import TestCase
from feedbackloop.main import create_app, ingest
from feedbackloop.app import db
from feedbackloop.test import TEST_MESSAGE
from feedbackloop.model import FeedbackEntry


class IngestTest(TestCase):

    def create_app(self):
        return create_app({
            'TESTING': True,
            'IMAP_SERVER': 'localhost',
            'IMAP_USERNAME': 'user',
            'IMAP_PASSWORD': 'password',
        })

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def assertExitStatus(self, exit_status, fn, *args, **kwargs):
        try:
            fn(*args, **kwargs)
        except SystemExit as exc:
            self.assertEqual(exit_status, exc.code)
            return
        raise AssertionError('exit was not called')

    @mock.patch('imaplib.IMAP4_SSL')
    def test_ingest_nothing(self, imap_mock):
        imap_mock.return_value.uid.return_value = ('OK', [b''])

        self.assertExitStatus(0, ingest, ['--limit', '10'])

    @mock.patch('imaplib.IMAP4_SSL')
    def test_ingest_onemsg(self, imap_mock):
        imap_mock.return_value.uid.side_effect = [
            ('OK', [b'42']),
            ('OK', [[None, TEST_MESSAGE]]),
            ('OK', []),
        ]

        self.assertExitStatus(0, ingest, [])

        entry = FeedbackEntry.query.filter(
            FeedbackEntry.sender=='foo@example.net').first()
        self.assertTrue(entry)
